package br.murilo.springbootsample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

	@RequestMapping("/")
	@ResponseBody
	public String index() {
		return "<html><body><h1>HelloWorld</h1></body></html>";
	}
	
}
